/* Copyright (c) 2011 by Alexandre Lissy. Published under the Clear BSD license.
 * Based on OpenLayers.Control.LayerSwitcher and OpenLayers.Control.OverviewMap
 * See http://svn.openlayers.org/trunk/openlayers/license.txt for the
 * full text of the license. */

/**
 * @requires OpenLayers/Control.js
 * @requires OpenLayers/Lang.js
 * @requires Rico/Corner.js
 **/

/**
 * Class: OpenLayers.Control.Legend
 * The Legend control displays a legend for data displayed in the map
 *
 * Inherits from:
 * - <OpenLayers.Control>
 */

OpenLayers.Control.Legend =
	OpenLayers.Class(OpenLayers.Control, {
		/**
		 * APIProperty: roundedCorner
		 * {Boolean} If true the Rico library is used for rounding the corners
		 *	 of the layer switcher div, defaults to true.
		 */
		roundedCorner: true,

		/**  
		 * APIProperty: roundedCornerColor
		 * {String} The color of the rounded corners, only applies if roundedCorner
		 *	 is true, defaults to "darkblue".
		 */
		roundedCornerColor: "darkblue",

		/**
		 * Property: legendValues
		 * 
		 * {Array(Object)} Specifying the legend colors and corresponding
		 *	values to display.
		 */
		legendValues: null,

		/**
		 * Property: minValue
		 * 
		 * {Integer} Minimum value of the data interval
		 */
		minValue: 0,

		/**
		 * Property: maxValue
		 * 
		 * {Integer} Maximum value of the data interval
		 */
		maxValue: 0,

		/**
		 * Property: errorValue
		 * 
		 * {Integer} Error value in the data interval. This is an 'expected' error
		 *	that should be displayed but separate from other values.
		 */
		errorValue: 0,

		/**
		 * Property: unitValue
		 * 
		 * {String} Unit for the values of the data interval
		 */
		unitValue: "",

		/**
		 * Property: scaleSize
		 * 
		 * {Integer} Number of elements to display
		 */
		scaleSize: 10,

		/**
		 * Property: colorFunction
		 * 
		 * {Function} Function which computes a color for a given value
		 *	function(min, max, value)
		 */
		colorFunction: null,

	// DOM Elements

		/**
		 * Property: minimizeDiv
		 * {DOMElement}
		 */
		minimizeDiv: null,

		/**
		 * Property: maximizeDiv
		 * {DOMElement}
		 */
		maximizeDiv: null,

		/**
		 * Constructor: OpenLayers.Control.Legend
		 * 
		 * Parameters:
		 * options - {Object}
		 */
		initialize: function(options) {
			OpenLayers.Control.prototype.initialize.apply(this, arguments);
			this.legendValues = [];
			this.minValue = options.minValue;
			this.maxValue = options.maxValue;
			this.colorFunction = options.colorFunction;
			this.scaleSize = options.scaleSize;
			this.unitValue = options.unitValue;
			this.errorValue = options.errorValue;

			this.need_redraw = false;

			this.recompute();
		},

		/**
		 * APIMethod: destroy
		 */
		destroy: function() {
			OpenLayers.Event.stopObservingElement(this.div);
			OpenLayers.Event.stopObservingElement(this.minimizeDiv);
			OpenLayers.Event.stopObservingElement(this.maximizeDiv);
			OpenLayers.Control.prototype.destroy.apply(this, arguments);
		},

		/**
		 * Method: recompute
		 *
		 * Recomputes all data
		 */
		recompute: function() {
			var scaleIncrement = (this.maxValue - this.minValue) / this.scaleSize;

			this.legendValues.push({
				value: this.errorValue,
				intlow: this.errorValue,
				inthigh: this.errorValue,
				color: this.colorFunction(this.minValue, this.maxValue, this.errorValue),
				iserror: true
			});

			for (var iValue = this.minValue; iValue <= this.maxValue; iValue += scaleIncrement) {
				var highValue = iValue + scaleIncrement

				if (iValue >= this.maxValue) {
					highValue = "&infin;";
				}

				this.legendValues.push({
					value: iValue,
					intlow: iValue,
					inthigh: highValue,
					color: this.colorFunction(this.minValue, this.maxValue, iValue),
					iserror: false
				});
			}

			this.need_redraw = true;
		},

		/**
		 * Method: draw
		 *
		 * Returns:
		 * {DOMElement} A reference to the DIV DOMElement containing the 
		 *	 switcher tabs.
		 */  
		draw: function() {
			OpenLayers.Control.prototype.draw.apply(this);

			// create layout divs
			this.loadContents();

			// set mode to minimize
			if(!this.outsideViewport) {
				this.minimizeControl();
			}

			// populate div with current info
			this.redraw();	

			return this.div;
		},

		/**
		 * Method: checkRedraw
		 * Checks if the layer state has changed since the last redraw() call.
		 * 
		 * Returns:
		 * {Boolean} The layer state changed since the last redraw() call. 
		 */
		checkRedraw: function() {
			return this.need_redraw;
		},
		
		/** 
		 * Method: redraw
		 * Loops through the values for the legend and display them.
		 *
		 * Returns: 
		 * {DOMElement} A reference to the DIV DOMElement containing the control
		 */  
		redraw: function() {
			//if the state hasn't changed since last redraw, no need 
			// to do anything. Just return the existing div.
			if (!this.checkRedraw()) { 
				return this.div; 
			}

			var legendList = document.createElement("ul");

			for (var id in this.legendValues) {
				var element = this.legendValues[id];
				var low = element.intlow;
				var high = element.inthigh;

				if (!isNaN(low)) {
					low = low.toFixed(2);
				}

				if (!isNaN(high)) {
					high = high.toFixed(2);
				}

				var legendItemColor = document.createElement("div");
				legendItemColor.style.backgroundColor = element.color;
				legendItemColor.innerHTML = "[&nbsp;" + low + "&nbsp;;&nbsp;" + high + "&nbsp;[&nbsp;" + this.unitValue;

				var legendItem = document.createElement("li");
				legendItem.appendChild(legendItemColor);
				OpenLayers.Element.addClass(legendItem, "legendItem");

				legendList.appendChild(legendItem);
			}

			OpenLayers.Element.addClass(legendList, this.displayClass + "List");
			this.legendDiv.appendChild(legendList);

			return this.div;
		},
    
		/** 
		 * Method: loadContents
		 * Set up the labels and divs for the control
		 */
		loadContents: function() {

			//configure main div
			OpenLayers.Event.observe(this.div, "mouseup", 
				OpenLayers.Function.bindAsEventListener(this.mouseUp, this));
			OpenLayers.Event.observe(this.div, "click",
						  this.ignoreEvent);
			OpenLayers.Event.observe(this.div, "mousedown",
				OpenLayers.Function.bindAsEventListener(this.mouseDown, this));
			OpenLayers.Event.observe(this.div, "dblclick", this.ignoreEvent);

			// legend div
			this.legendDiv = document.createElement("div");
			this.legendDiv.id = this.id + "_legendDiv";
			OpenLayers.Element.addClass(this.legendDiv, "legendDiv");

			this.legendLbl = document.createElement("div");
			this.legendLbl.innerHTML = OpenLayers.i18n("Legend");
			OpenLayers.Element.addClass(this.legendLbl, "legendLbl");
			
			this.legendValuesDiv = document.createElement("div");
			OpenLayers.Element.addClass(this.legendValuesDiv, "legendValuesDiv");

			this.legendDiv.appendChild(this.legendLbl);
			this.legendDiv.appendChild(this.legendValuesDiv);

			this.div.appendChild(this.legendDiv);

			if(this.roundedCorner) {
				OpenLayers.Rico.Corner.round(this.div, {
					corners: "tl bl",
					bgColor: "transparent",
					color: this.roundedCornerColor,
					blend: false
				});
				OpenLayers.Rico.Corner.changeOpacity(this.legendDiv, 0.75);
			}

			if (!this.outsideViewport) {
				this.div.className += " " + this.displayClass + 'Container';

				var imgLocation = OpenLayers.Util.getImagesLocation();
				var sz = new OpenLayers.Size(18,18);		

				// maximize button div
				var img = imgLocation + 'layer-switcher-maximize.png';
				this.maximizeDiv = OpenLayers.Util.createAlphaImageDiv(
											this.displayClass + "MaximizeDiv", 
											null, 
											sz, 
											img, 
											"absolute");
				OpenLayers.Element.addClass(this.maximizeDiv, this.displayClass + "MaximizeButton");
				this.maximizeDiv.style.display = "none";
				OpenLayers.Event.observe(this.maximizeDiv, "click", 
					OpenLayers.Function.bindAsEventListener(this.maximizeControl, this)
				);
				
				this.div.appendChild(this.maximizeDiv);

				// minimize button div
				var img = imgLocation + 'layer-switcher-minimize.png';
				var sz = new OpenLayers.Size(18,18);		
				this.minimizeDiv = OpenLayers.Util.createAlphaImageDiv(
											this.displayClass + "MinimizeDiv", 
											null, 
											sz, 
											img, 
											"absolute");
				OpenLayers.Element.addClass(this.minimizeDiv, this.displayClass + "MinimizeButton");
				this.minimizeDiv.style.display = "none";
				OpenLayers.Event.observe(this.minimizeDiv, "click", 
					OpenLayers.Function.bindAsEventListener(this.minimizeControl, this)
				);

				this.div.appendChild(this.minimizeDiv);
			}
		},

		/** 
		 * Method: maximizeControl
		 * Set up the labels and divs for the control
		 * 
		 * Parameters:
		 * e - {Event} 
		 */
		maximizeControl: function(e) {

			// set the div's width and height to empty values, so
			// the div dimensions can be controlled by CSS
			this.div.style.width = "";
			this.div.style.height = "";

			this.showControls(false);

			if (e != null) {
				OpenLayers.Event.stop(e);											
			}
		},
    
		/** 
		 * Method: minimizeControl
		 * Hide all the contents of the control, shrink the size, 
		 *     add the maximize icon
		 *
		 * Parameters:
		 * e - {Event} 
		 */
		minimizeControl: function(e) {

			// to minimize the control we set its div's width
			// and height to 0px, we cannot just set "display"
			// to "none" because it would hide the maximize
			// div
			this.div.style.width = "0px";
			this.div.style.height = "0px";

			this.showControls(true);

			if (e != null) {
			    OpenLayers.Event.stop(e);                                            
			}
		},

		/**
		 * Method: showControls
		 * Hide/Show all LayerSwitcher controls depending on whether we are
		 *     minimized or not
		 * 
		 * Parameters:
		 * minimize - {Boolean}
		 */
		showControls: function(minimize) {

			this.maximizeDiv.style.display = minimize ? "" : "none";
			this.minimizeDiv.style.display = minimize ? "none" : "";

			this.legendDiv.style.display = minimize ? "none" : "";
		},
    
		/** 
		 * Method: ignoreEvent
		 * 
		 * Parameters:
		 * evt - {Event} 
		 */
		ignoreEvent: function(evt) {
			OpenLayers.Event.stop(evt);
		},

		/** 
		 * Method: mouseDown
		 * Register a local 'mouseDown' flag so that we'll know whether or not
		 *	 to ignore a mouseUp event
		 * 
		 * Parameters:
		 * evt - {Event}
		 */
		mouseDown: function(evt) {
			this.isMouseDown = true;
			this.ignoreEvent(evt);
		},

		/** 
		 * Method: mouseUp
		 * If the 'isMouseDown' flag has been set, that means that the drag was 
		 *	 started from within the LayerSwitcher control, and thus we can 
		 *	 ignore the mouseup. Otherwise, let the Event continue.
		 *  
		 * Parameters:
		 * evt - {Event} 
		 */
		mouseUp: function(evt) {
			if (this.isMouseDown) {
				this.isMouseDown = false;
				this.ignoreEvent(evt);
			}
		},

		CLASS_NAME: "OpenLayers.Control.Legend"
	});
