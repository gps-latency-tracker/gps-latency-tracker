import sys
import os
import signal
import threading
import time
import GLT.gps, GLT.result, GLT.savers, GLT.conf

class GLTMain:
	def __init__(self):
		self.measureWorkers = []
		self.measureId = 0
		self.conf = GLT.conf.GLTConf()
		self.conf.parse()
		self.handler = self.conf.getHandler()
		self.start()
	
	def start(self):
		print "Installing signals handlers"
		signal.signal(signal.SIGINT, self.sighandler)
		signal.signal(signal.SIGTERM, self.sighandler)
		signal.signal(signal.SIGUSR1, self.sighandler)
		print "Signals handlers installed."
		try:
			self.sv = GLT.savers.Saver(self.conf.getConfValue("saverhandler"), self.conf.getConfValue("saverparams"), [ self.conf.getConfValue("measure"), self.conf.getConfValue("measureparams"), self.conf.getConfValue("measurefreq") ])
		except GLT.savers.SaverFileExists:
			print "Output file already exists, and no --overwrite flag given. Aborting."
			sys.exit(1)

	def setup_alarm(self):
		print "Installing alarm."
		signal.signal(signal.SIGALRM, self.launcher)
		signal.siginterrupt(signal.SIGALRM, False)
		signal.setitimer(signal.ITIMER_REAL, 5, self.conf.getConfValue("measurefreq"))
		print "Alarm installed and started."

	def get_device_string(self):
		mode = self.conf.getConfValue("mode")
		host = self.conf.getConfValue("hostname") + ":" + str(self.conf.getConfValue("tcpport"))
		dev = self.conf.getConfValue("bluetoothmac")
		if mode == "tcp":
			return mode + "://" + host
		elif mode == "bluetooth":
			return mode + "://" + dev
		else:
			return "none://"
	
	def run(self):
		print "Starting GPS Thread with ", self.get_device_string()
		
		try:
			self.gpsthread = GLT.gps.WorkerGps(self.get_device_string())
		except GLT.gps.WorkerGpsServicesDiscoveryException as e:
			if e.errno == GLT.gps.WorkerGpsServicesDiscoveryException.ERROR_SERVICESDISCOVERY_NOSERVICE:
				print >> sys.stderr, "No Bluetooth GPS discovered, aborting ..."
			if e.errno == GLT.gps.WorkerGpsServicesDiscoveryException.ERROR_SERVICESDISCOVERY_TOOMANYSERVICES:
				print >> sys.stderr, "Too many Bluetooth GPS discovered, check your config ..."
			self.suicide()
		
		self.setup_alarm()			
		self.gpsthread.daemon = True
		self.gpsthread.start()
		print "GPS Thread started."
		while True:
			if self.gpsthread.networkError:
				self.suicide()
			time.sleep(0.1)
		self.gpsthread.join()
		self.suicide()

	# This is not Rock'n'Roll, this is suicide!!
	def suicide(self):
		os.kill(os.getpid(), signal.SIGUSR1)

	def sighandler(self, signum, frame):
		retcode = 0
		if signum == signal.SIGINT:
			print "\nGot SIGINT, leaving ..."
		elif signum == signal.SIGTERM:
			print "Got SIGTERM, leaving !"
		elif signum == signal.SIGUSR1:
			print "Suicide requested ..."
		else:
			raise Exception("Unhandled signal: ", signum)

		print "Disabling SIGALRM"
		signal.signal(signal.SIGALRM, signal.SIG_IGN)

		print "Stoping GPS thread ..."
		self.gpsthread.stopThread = True
		self.gpsthread.join(1)

		print "Waiting {0} sec(s) for each alive workers threads to finish ... ".format(self.conf.getConfValue("threadtimeout"))
		for worker in self.measureWorkers:
			if worker.is_alive():
				print "  Thread '{0}' still alive, asking to join()".format(worker.name)
				worker.join(self.conf.getConfValue("threadtimeout"))

		self.sv.finish()
		sys.exit(retcode)

	def launcher(self, signum, frame):
		if self.gpsthread.gps_ready:
			wmr = GLT.result.WorkerMeasureResult(self.gpsthread, self.sv, self.handler, self.conf.getConfValue("measureparams"))
			wmr.name = "Measure " + str(self.measureId)
			self.measureWorkers.append(wmr)
			wmr.start()
			self.measureId += 1

		for worker in self.measureWorkers:
			if not worker.is_alive():
				self.measureWorkers.remove(worker)

if __name__ == '__main__':
	glt = GLTMain()
	glt.run()
