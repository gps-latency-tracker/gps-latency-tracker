import sys
import os
import ping
import argparse
import re

class GLTConfParameterException(Exception):
	ERROR_GLTCONF_INVALIDMACBLUETOOTH	= 1
	ERROR_GLTCONF_INVALIDMEASUREHANDL	= 2
	ERROR_GLTCONF_INVALIDFREQUENCY		= 3
	ERROR_GLTCONF_INVALIDSAVER		= 4
	ERROR_GLTCONF_INVALIDHOSTNAME		= 5
	ERROR_GLTCONF_INVALIDNETWORK		= 6
	ERROR_GLTCONF_INVALIDTIMEOUT		= 7
	ERROR_GLTCONF_PARAMETERUNKNOWN		= 8

	def __init__(self, errno, ivalue):
		Exception.__init__(self, ivalue)
		self.value = ivalue
		self.errno = errno
	
	def __str__(self):
		return self.value, ": ", self.errno

class GLTConf:
	def __init__(self):
		self.measureHandlers = {
			'ping': ping
		}

		self.params = {
			"mode": "tcp",
			"hostname": '',
			"tcpport": 4352,
			"bluetoothmac": '',
			"measure": '',
			"measurefreq": '',
			"measureparams": [],
			"saverhandler": '',
			"saverparams": {},
			"threadtimeout": 5
		}

		self.argparser = argparse.ArgumentParser(description="Doing network measures and correlate them with GPS coordinates.")
		self.argparser.add_argument("--mode", default="tcp", help="Communication mode with GPS. Pass 'bluetooth' for Bluetooth. Default: 'tcp'.")
		self.argparser.add_argument("--host", required=True, help="Network address of the GPS. If using Bluetooth, this is the MAC address. If using TCP, this is host:port.")
		self.argparser.add_argument(
			'--output', required=True, default="glt.csv",
			help="File to be used by Saver. THe correct saver type to be used  will be infered from file's extension. Default: 'glt.csv'."
		)
		self.argparser.add_argument(
			"--measure", default="ping", help="Selecting a network measure handler. Default: 'ping'."
		)
		self.argparser.add_argument(
			"--frequency", type=int, default=15,
			help="How often (in seconds) should SIGARLM be sent to GLT.launcher() to get a new measure. Default: '15'."
		)
		self.argparser.add_argument(
			"--params", help="Comma-separated string of additionnal parameters for the measure handler. Will be exploded to an array, directly passed to the measure handler."
		)
		self.argparser.add_argument(
			"--overwrite", action="store_true", default=False, dest="overwrite", help="If the output file already exists, overwrite it."
		)
		self.argparser.add_argument(
			"--threadtimeout", type=int, default=5,
			help="How long (seconds) should we wait for each thread to complete when receiving SIGINT. This will be used as a parameter to thread.join() method. Default: '5'."
		)
		self.args = self.argparser.parse_args()
	
	def parse(self):
		if self.args.mode:
			if self.args.mode != "tcp" and self.args.mode != "bluetooth":
				raise GLTConfParameterException(GLTConfParameterException.ERROR_GLTCONF_INVALIDNETWORK, "Invalid network: not TCP or Bluetooth")
			else:
				self.params["mode"] = self.args.mode

		if self.args.host:
			if self.args.mode == "tcp":
				r = re.compile("([^\"]+)(:[0-9]+)?")
				if r.search(self.args.host):
					split = self.args.host.split(":")
					self.params["hostname"] = split[0]
					if len(split) > 1:
						self.params["tcpport"] = split[1]
				else:
					raise GLTConfParameterException(GLTConfParameterException.ERROR_GLTCONF_INVALIDHOSTNAME, "Invalid Hostname/Port")
			elif self.args.mode == "bluetooth":
				r = re.compile("[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}")
				if r.match(self.args.host):
					self.params["bluetoothmac"] = self.args.host
				else:
					raise GLTConfParameterException(GLTConfParameterException.ERROR_GLTCONF_INVALIDMACBLUETOOTH, "Invalid MAC Bluetooth")

		if self.args.measure:
			if self.args.measure in self.measureHandlers:
				self.params["measure"] = self.args.measure
			else:
				raise GLTConfParameterException(GLTConfParameterException.ERROR_GLTCONF_INVALIDMEASUREHANDL, "Invalid measuer handler")

		if self.args.params:
			self.params["measureparams"] = self.args.params.split(",")

		if self.args.frequency:
			if self.args.frequency > 0:
				self.params["measurefreq"] = self.args.frequency
			else:
				raise GLTConfParameterException(GLTConfParameterException.ERROR_GLTCONF_INVALIDFREQUENCY, "Invalid frequency")

		if self.args.output:
			r = re.compile(".*\.([^\"]+)$")
			s = r.search(self.args.output)
			if s:
				self.params["saverhandler"] = s.group(1)
				self.params["saverparams"] = { 'file': os.path.realpath(self.args.output) }
			else:
				raise GLTConfParameterException(GLTConfParameterException.ERROR_GLTCONF_INVALIDSAVER, "Cannot match saver")

		if self.args.overwrite:
			self.params["saverparams"]["force"] = self.args.overwrite

		if self.args.threadtimeout:
			if self.args.threadtimeout >= 0:
				self.params["threadtimeout"] = self.args.threadtimeout
			else:
				raise GLTConfParameterException(GLTConfParameterException.ERROR_GLTCONF_INVALIDTIMEOUT, "Invalid thread timeout")

	def getConfValue(self, name):
		try:
			self.params[name]
		except KeyError as e:
			print "Unknown parameter: {0}".format([name])
			raise GLTConfParameterException(GLTConfParameterException.ERROR_GLTCONF_PARAMETERUNKNOWN, "Unknown parameter: {0}".format([name]))
		return self.params[name]

	def getHandlers(self):
		return self.measureHandlers

	def getHandler(self):
		return self.measureHandlers[self.getConfValue("measure")]
