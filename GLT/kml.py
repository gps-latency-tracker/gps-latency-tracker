import xml.dom.minidom

class GLTSaver:
	def __init__(self, params, measure):
		self.params = params
	
	def start(self):
		self.kmlDoc = xml.dom.minidom.Document()
		kmlElement = self.kmlDoc.createElementNS('http://earth.google.com/kml/2.2', 'kml')
		kmlElement.setAttribute('xmlns', 'http://earth.google.com/kml/2.2')
		kmlElement = self.kmlDoc.appendChild(kmlElement)
		self.documentElement = self.kmlDoc.createElement('Document')
		self.documentElement = kmlElement.appendChild(self.documentElement)

	def create_dataElement(self, name, value):	
		dataElement = self.kmlDoc.createElement('Data')
		dataElement.setAttribute('name', name)
		valueElement = self.kmlDoc.createElement('value')
		dataElement.appendChild(valueElement)
		valueText = self.kmlDoc.createTextNode(str(value))
		valueElement.appendChild(valueText)
		return dataElement

	def save(self, result):
		placemarkElement = self.kmlDoc.createElement('Placemark')
		extElement = self.kmlDoc.createElement('ExtendedData')
		placemarkElement.appendChild(extElement)

		extElement.appendChild(self.create_dataElement("time", result["gpsvalues"]["time"]))
		extElement.appendChild(self.create_dataElement("speed", result["gpsvalues"]["gps"]["speed"]))
		extElement.appendChild(self.create_dataElement("angle", result["gpsvalues"]["gps"]["angle"]))
		extElement.appendChild(self.create_dataElement("magnetic", result["gpsvalues"]["gps"]["magnetic"]))
		extElement.appendChild(self.create_dataElement("unit", result["measure"]["unit"]))
		extElement.appendChild(self.create_dataElement("value", result["measure"]["value"]))
		extElement.appendChild(self.create_dataElement("error", result["measure"]["error"]))

		pointElement = self.kmlDoc.createElement('Point')
		placemarkElement.appendChild(pointElement)
		coordinates = str(result["gpsvalues"]["gps"]["longitude"]) + "," + str(result["gpsvalues"]["gps"]["latitude"]) + "," + str(result["gpsvalues"]["gps"]["altitude"])
		coorElement = self.kmlDoc.createElement('coordinates')
		coorElement.appendChild(self.kmlDoc.createTextNode(coordinates))
		pointElement.appendChild(coorElement)
		self.documentElement.appendChild(placemarkElement)

	def finish(self):
		self.params['fd'].write(self.kmlDoc.toprettyxml('  ', newl = '\n', encoding = 'utf-8'))
