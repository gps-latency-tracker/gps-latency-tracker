import xml.dom.minidom
import datetime

class GLTSaver:
	def __init__(self, params, measure):
		self.params = params
		self.method = measure[0]
		self.methodparams = measure[1]
		self.methodfreq = measure[2]
	
	def start(self):
		self.gpxDoc = xml.dom.minidom.Document()
		gpxElement = self.gpxDoc.createElementNS('http://www.topografix.com/GPX/1/1', 'gpx')
		gpxElement.setAttribute('xmlns', 'http://www.topografix.com/GPX/1/1')
		gpxElement.setAttribute('version', '1.1')
		gpxElement.setAttribute('creator', 'glt')
		gpxElement.setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
		gpxElement.setAttribute('xsi:schemaLocation', 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd')
		gpxElement.setAttribute('xmlns:glt', 'https://gitorious.org/gps-latency-tracker/gps-latency-tracker/')
		gpxElement.appendChild(self.create_gpx_metadata())
		self.documentElement = gpxElement
		self.gpxDoc.appendChild(self.documentElement)

	def create_gpx_metadata(self):
		metadataElement = self.gpxDoc.createElement("metadata")
		metadataElement.appendChild(self.create_node("name", self.method))
		metadataElement.appendChild(self.create_node("desc", "Tracking '{0}' measure with parameters '{1}' every {2} sec and correlate with GPS coordinates".format(self.method, self.methodparams, self.methodfreq)))
		metadataElement.appendChild(self.create_node("time", datetime.datetime.now().isoformat()))
		return metadataElement

	def create_glt_extensions(self, unit, value, error):
		extensionsElement = self.gpxDoc.createElement("extensions")
		extensionsElement.appendChild(self.create_node("unit", str(unit)))
		extensionsElement.appendChild(self.create_node("value", str(value)))
		extensionsElement.appendChild(self.create_node("error", str(error)))
		return extensionsElement

	def create_node(self, name, value):	
		valueElement = self.gpxDoc.createElement(name)
		valueText = self.gpxDoc.createTextNode(str(value))
		valueElement.appendChild(valueText)
		return valueElement

	def save(self, result):
		lat = result["gpsvalues"]["gps"]["latitude"]
		lon = result["gpsvalues"]["gps"]["longitude"]
		waypointElement = self.gpxDoc.createElement('wpt')
		waypointElement.setAttribute("lat", str(lat))
		waypointElement.setAttribute("lon", str(lon))

		waypointElement.appendChild(self.create_node("ele", result["gpsvalues"]["gps"]["altitude"]))
		waypointElement.appendChild(self.create_node("time", datetime.datetime.fromtimestamp(result["gpsvalues"]["time"]).isoformat()))
		magvar = result["gpsvalues"]["gps"]["magnetic"]
		if len(magvar) == 0:
			magvar = "0.0"
		waypointElement.appendChild(self.create_node("magvar", magvar))
		waypointElement.appendChild(self.create_glt_extensions(result["measure"]["unit"], result["measure"]["value"], result["measure"]["error"]))

		self.documentElement.appendChild(waypointElement)

	def finish(self):
		self.params['fd'].write(self.gpxDoc.toxml(encoding = 'utf-8'))
