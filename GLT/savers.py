import csv
import kml
import gpx
import os.path

class SaverUnkownMethod(Exception):
	def __init__(self, ivalue):
		Exception.__init__(self, ivalue)

class SaverFileExists(Exception):
	def __init__(self, ivalue):
		Exception.__init__(self, ivalue)

class Saver:
	def __init__(self, method, params, measure):
		self.method = method
		self.params = params
		self.methods = {
			'csv': csv,
			'kml': kml,
			'gpx': gpx
		}
		self.check()
		self.handler = self.methods[self.method].GLTSaver(self.params, measure)
		self.start()

	def check(self):
		found = False
		
		for method in self.methods:
			if self.method == method:
				found = True

		if found == False:
			raise SaverUnkownMethod("Method '", self.method, "' unknown")
	
	def start(self):
		if os.path.isfile(self.params['file']) and not 'force' in self.params:
			raise SaverFileExists("File already exists and no force passed.");

		self.params['fd'] = open(self.params['file'], 'w')
		self.handler.start()
		self.params['fd'].flush()
	
	def save(self, result):
		self.handler.save(result)
		self.params['fd'].flush()
	
	def finish(self):
		self.handler.finish()
		self.params['fd'].flush()
		self.params['fd'].close()
