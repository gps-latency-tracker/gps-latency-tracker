import sys
import subprocess
import re

class Measure:
	def __init__(self, target):
		self.params = []
		self.searchRegex = re.compile("time=(.*) ms$")
		self.params.append("ping")
		if len(target) > 1:
			self.params.append("-s " + target[1])
		self.params.append("-c 1")
		self.params.append(target[0])
		self.unit = "ms"

	def run(self):
		try:
			ret = subprocess.check_output(self.params)
		except subprocess.CalledProcessError:
			return (-1, True)

		for l in ret.splitlines(1):
			l = l.strip()
			res = self.searchRegex.search(l)
			if res:
				return (res.group(1), False)
		
		raise Exception("Cannot get time from ping output")
