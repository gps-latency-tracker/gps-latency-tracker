class GLTSaver:
	def __init__(self, params, measure):
		self.params = params

	def start(self):
		csvhead = '"time", "latitude", "longitude", "altitude", "speed", "angle", "magnetic", "unit", "value", "error"\n'
		self.params['fd'].write(csvhead)
	
	def save(self, result):
		values = {'time': result["gpsvalues"]["time"],
			'latitude': result["gpsvalues"]["gps"]["latitude"],
			'longitude': result["gpsvalues"]["gps"]["longitude"],
			'altitude': result["gpsvalues"]["gps"]["altitude"],
			'speed': result["gpsvalues"]["gps"]["speed"],
			'angle': result["gpsvalues"]["gps"]["angle"],
			'magnetic': result["gpsvalues"]["gps"]["magnetic"],
			'unit': result["measure"]["unit"],
			'value': result["measure"]["value"],
			'error': result["measure"]["error"] }
		csvline = '"{time}", "{latitude}", "{longitude}", "{altitude}", "{speed}", "{angle}", "{magnetic}", "{unit}", "{value}", "{error}"\n'.format(**values)
		self.params['fd'].write(csvline)

	def finish(self):
		pass
