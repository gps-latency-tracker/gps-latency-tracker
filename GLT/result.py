import gps
import threading

class WorkerMeasureResult(threading.Thread):
	def __init__(self, gps, saver, measure, target):
		threading.Thread.__init__(self)
		self.gpsthread = gps
		self.measure = measure
		self.target = target
		self.result = {}
		self.saver = saver
		self.daemon = True

	def run(self):
		self.tryMeasureAndSave()
		return True

	def tryMeasure(self):
		try:
			gpsvalue = self.gpsthread.get_last_gps_data()
		except gps.WorkerGpsExceptionGpsData:
			return False

		measure = self.measure.Measure(self.target)
		# Error here is "something failed but it's an indication of the measure
		# so it's a 'valid' error"
		(v, e) = measure.run()
		result = { 'unit': measure.unit, 'value': v, 'error': e }
		self.result = {'gpsvalues': gpsvalue, 'measure': result}

		return True
	
	def tryMeasureAndSave(self):
		if self.tryMeasure():
			self.saver.save(self.result)
	
	def __str__(self):
		return str(self.result)
