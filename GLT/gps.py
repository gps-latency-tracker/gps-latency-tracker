import sys
import bluetooth
import socket
import threading
import time
import errno
import re

class WorkerGpsServicesDiscoveryException(Exception):
	ERROR_SERVICESDISCOVERY_NOSERVICE = 1
	ERROR_SERVICESDISCOVERY_TOOMANYSERVICES = 2

	def __init__(self, errno, ivalue):
		Exception.__init__(self, ivalue)
		self.value = ivalue
		self.errno = errno
	
	def __str__(self):
		return self.value, ": ", self.errno

class WorkerGpsExceptionGpsData(Exception):
	ERROR_GPSDATA_TOOOLD = 1
	ERROR_GPSDATA_TOOMUCH = 2

	def __init__(self, errno, ivalue):
		Exception.__init__(self, ivalue)
		self.value = ivalue
		self.errno = errno
	
	def __str__(self):
		return self.value, ": ", self.errno

class WorkerGps(threading.Thread):
	def __init__(self, device, maxold = 15):
		threading.Thread.__init__(self, name="GPS Data Thread")

		self.gpsBuffer = {}
		self.gpsBufferLock = threading.Lock()
		self.stopThread = False
		self.device = device
		self.maxold = maxold
		self.networkError = False
		self.set_gps_ready = False

		## latitude: "4728.1143N"
		## longitude: "00055.1039E"
		self.nmea_re_lat = re.compile("([0-9]{2})([0-9]{2}\.[0-9]{4})(N|S)$")
		self.nmea_re_long = re.compile("([0-9]{3})([0-9]{2}\.[0-9]{4})(E|W)$")
		## distance: "545.9M"
		self.nmea_re_dist = re.compile("([^\"]+)(m|M)")

		regex = re.compile("([^\"]+)://([^\"]+)")
		self.devices = regex.search(self.device)

		self.sock = self.connect_to_gps()

	def discover_gps_channel(self):
		services = bluetooth.find_service(uuid="00001101-0000-1000-8000-00805f9b34fb", address=self.devices.group(2))

		if len(services) > 1:
			raise WorkerGpsServicesDiscoveryException(WorkerGpsServicesDiscoveryException.ERROR_SERVICESDISCOVERY_TOOMANYSERVICES, "Too many services discovered over Bluetooth")
		if len(services) == 1:
			return services[0]["port"]
		else:
			raise WorkerGpsServicesDiscoveryException(WorkerGpsServicesDiscoveryException.ERROR_SERVICESDISCOVERY_NOSERVICE, "No matching service discovered over Bluetooth")

	def connect_to_gps(self):
		if self.devices.group(1) == "bluetooth":
			port = self.discover_gps_channel()
			sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
			host = self.devices.group(2)
		elif self.devices.group(1) == "tcp":
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			val = self.devices.group(2).split(":")
			host = val[0]
			port = int(val[1])
		else:
			sock = None

		sock.connect((host, port))
		return sock
	
	def run(self):
		while not self.stopThread:
			self.get_gps_data()
		self.daemon = False
		return

	def is_gps_ready(self, coords):
		return (coords['status'] == 'A')

	def setgpsready(self):
		self.gps_ready = True

	def setgpsnotready(self):
		self.gps_ready = False

	def get_last_gps_data(self):
		ctime = time.time()
		self.gpsBufferLock.acquire()
		if len(self.gpsBuffer) > 0:
			value = self.gpsBuffer[len(self.gpsBuffer) - 1]
			old = ctime - value['time']
			if old > self.maxold:
				self.gpsBufferLock.release()
				raise WorkerGpsExceptionGpsData(WorkerGpsExceptionGpsData.ERROR_GPSDATA_TOOOLD, "Too old")
		else:
			self.gpsBufferLock.release()
			raise WorkerGpsExceptionGpsData(WorkerGpsExceptionGpsData.ERROR_GPSDATA_TOOMUCH, "No data")

		self.gpsBufferLock.release()
		return value

	def get_gps_data(self):
		anim = [ '|', '/', '-', '\\' ]
		animItem = 0
		for coords in self.get_nmea_data():
			if not self.stopThread:
				if (self.is_gps_ready(coords)):
					self.setgpsready()
					print "\r[GPS] Getting data ", anim[animItem % len(anim)],
					self.gpsBufferLock.acquire()
					self.gpsBuffer[0] = { 'time': time.time(), 'gps': coords }
					self.gpsBufferLock.release()
				else:
					self.setgpsnotready()
					print "\r[GPS] Wait fix ... ", anim[animItem % len(anim)], 
				animItem += 1
				sys.stdout.flush()

	def nmea_to_deg(self, latitude, longitude):
		## NMEA to Decimal
		## latitude = "4728.1143N" => 47 + (28.1143/60)
		## longitude = "00055.1039E" => 000 + (55.1039/60)
		latre = self.nmea_re_lat.search(latitude)
		longre = self.nmea_re_long.search(longitude)

		if (not latre) or (not longre):
			raise Exception("Unknwon NMEA data")

		return (float(latre.group(1)) + float(latre.group(2))/60, float(longre.group(1)) + float(longre.group(2))/60)

	def nmea_to_meter(self, value):
		meters = self.nmea_re_dist.search(value)
		group1 = float(meters.group(1))
		
		if not meters:
			raise Exception("Unknown NMEA distance data")

		unit = meters.group(2)
		if (unit == "m" or unit == "M"):
			retval = group1
		else:
			retval = group1

		return float(retval)

	def knots_to_ms(self, knots):
		return float(knots)*(1.852/3.6)

	# from http://blog.cedricbonhomme.org/2011/02/17/recuperer-les-donnees-gps-dandroid-en-temps-reel/
	def get_nmea_data(self):
		data, olddata = "", ""
		status = 'V'
		latitude, longitude = "", ""
		altitude = ""
		speed = 0.0
		angle = 0.0
		date = 0
		magnetic = ""

		while True and not self.networkError:
			try:
				data = self.sock.recv(1024)
			except bluetooth.BluetoothError as e:
				print "Bluetooth disconnected. Reason: ", e.strerror, " (errno=", e.errno, ")"
				self.networkError = True
				return

			if len(data) > 0:
				# append the old data to the front of data.
				data = olddata + data

				# split the data into a list of lines, but make
				# sure we preserve the end of line information.
				lines = data.splitlines(1)

				# iterate over each line
				for line in lines:
					# if the line has a carriage return and a
					# linefeed, we know we have a complete line so
					# we can remove those characters and print it.
					if line.find("\r\n") != -1 :
						line = line.strip()
						# empty the olddata variable now we have
						# used the data.
						olddata = ""

					# else we need to keep the line to add to data
					else :
						olddata = line

				## NMEA GPRMC reference: http://www.gpsinformation.org/dale/nmea.htm
				splitted = line.split('*')
				gpsstring = splitted[0].split(',')
				checksum = "*" + splitted[1].strip()
				if gpsstring[0] == '$GPRMC' :
					status = gpsstring[2]
					(latitude, longitude) = self.nmea_to_deg(gpsstring[3] + gpsstring[4], gpsstring[5] + gpsstring[6])
					speed = self.knots_to_ms(gpsstring[7])
					angle = gpsstring[8] ## track angle in degrees
					date = gpsstring[9]
					magnetic = gpsstring[10] + gpsstring[11]
				elif gpsstring[0] == '$GPGGA':
					(latitude, longitude) = self.nmea_to_deg(gpsstring[2] + gpsstring[3], gpsstring[4] + gpsstring[5])
					altitude = self.nmea_to_meter(gpsstring[9] + gpsstring[10])
			else:
				print "No data to read, probably network failure."
				self.networkError = True
				return

			yield { 'status': status, 'latitude': latitude, 'longitude': longitude, 'altitude': altitude, 'speed': speed, 'angle': angle, 'date': date, 'magnetic': magnetic }
